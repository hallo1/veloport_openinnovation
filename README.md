# veloport_openinnovation



## Getting started

Dieses Repository dient dazu, Feature - Requests und Fehlermeldungen zu sammeln und so das Velocom Team zu entlasten und dabei zu unterstützen, die richtigen Entscheidungen zu treffen. 

Wir möchten eine offene Community von Nutzern schaffen, und einen offenen Diskurs rund um die Software Veloport für den Fahrradhandel schaffen, damit Fragen oder Wünsche die einmal gestellt wurden auch von anderen Nutzern bewertet werden können - so ist es möglich festzustellen, ob ein Bedarf besteht oder wie unterschiedliche Nutzer Veloport nutzen oder nutzen möchten.   


## Installation
Hier gibt es keinen Source-Code und nichts herunterzuladen, dieses Repo dient nur dazu Issues zu erstellen und zu moderieren. 

## Usage
Gehe auf Issue und trage dort deine Fehlermeldung oder Frage an das Team ein.


## Moderation
Gerne kann noch jemand dazu beitragen Issues zu moderieren. 

## Roadmap
Wie soll Veloport in Zukunft aussehen, welche neuen Features sollen implementiert werden oder nicht? 
Gemeinsam wollen wir hier in einen Dialog mit den Entwicklern treten.
